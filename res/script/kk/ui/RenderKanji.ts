/// <reference path="../../defs/jquery/jquery.d.ts" />
/// <reference path="../../fg/gui/canvas/Animations.ts" />
/// <reference path="../../fg/dom/make.ts" />

module kk.ui {

    var kanjivgAreaSize = 109;

    export class RenderKanji {

        private static renderer: Renderer;

        private data: { [code: number]: Node };
        private queue: { args: any; settings: RendererSettings; }[] = [];

        constructor(kanjivg: string) {
            if(!RenderKanji.renderer)
                RenderKanji.renderer = new Renderer();

            $.ajax({
                url: kanjivg,
                dataType: 'xml',
                cache: true,
                success: (data: XMLDocument) => {
                    this.parseDatabase(data);

                    for(var i = 0; i < this.queue.length; i++) {
                        var item = this.queue[i];

                        var settings = RenderKanji.renderer.save();

                        RenderKanji.renderer.load(item.settings);
                        this.render(item.args);

                        RenderKanji.renderer.load(settings);
                    }
                    this.queue = null;
                },
                error: () => {
                    console.error("Failed to load KanjiVG database.");
                }
            });
        }

		public appendTo(el: JQuery, text: string, animation?: boolean) {
			var size = parseFloat(el.css('fontSize'));

            var settings = RenderKanji.renderer.save();

            RenderKanji.renderer.color = el.css('color');
            RenderKanji.renderer.weight = parseInt(el.css('fontWeight'));
            RenderKanji.renderer.x = 0;
            RenderKanji.renderer.y = 0;
            //console.debug("el.css('fontWeight')", el.css('fontWeight'), RenderKanji.renderer.weight);
            //console.debug("el.css('color')", el.css('color'), RenderKanji.renderer.color);

            var theSettings = RenderKanji.renderer.save();

            ((theSettings: RendererSettings) => {
                var readySymbols = 0;
                var onReady: Function[] = [];

                var container = fg.dom.make('div', {
                    style: {
                        display: 'inline-block',
                        cursor: 'text'
                    }
                });

                el.empty().append(container);
    			for(var i = 0; i < text.length; i++) {
    				var symbol = text.charAt(i);
    				var canvas = <HTMLCanvasElement>fg.dom.make('canvas', {
    					attr: {
    						width: size.toString(),
    						height: size.toString()
    					}
    				}).get(0);

                    var image = <HTMLImageElement>fg.dom.make('img', {
                        style: {
                            display: 'inline-block',
                            verticalAlign: 'baseline',
                            pointerEvents: 'none'
                        },
                        attr: {
                            width: canvas.width.toString(),
                            height: canvas.height.toString(),
                            alt: symbol,
                            src: canvas.toDataURL()
                        }
                    }).get(0);

                    if (i == 0)
                        container.append(image);

                    ((canvas: HTMLCanvasElement, image: HTMLImageElement, container: JQuery, size: number, symbol: string, i: number) => {
                        var x: number;

                        onReady.push(() => {
                            //return; // no shrink to fit, no animation

                            var settings = RenderKanji.renderer.save();
                            RenderKanji.renderer.load(theSettings);
                            RenderKanji.renderer.x = x;

                            this.render({
                                canvas: canvas,
                                size: size,
                                symbol: symbol,
                                animation: animation,
                                onDone: () => {
                                    var item = onReady.shift();
                                    if (item) {
                                        //console.debug("rendering symbol");
                                        item();
                                    }
                                },
                                mapTo: image
                            });

                            //var c2d = <CanvasRenderingContext2D>canvas.getContext('2d');
                            //c2d.beginPath();
                            //c2d.rect(bounds.x.min - 5, 0, bounds.x.max + 5, canvas.height);
                            //c2d.stroke();
                            ////console.log("bounds", JSON.stringify(bounds));

                            RenderKanji.renderer.load(settings);
                        });

                        this.render({
                            canvas: canvas,
                            size: size,
                            symbol: symbol,
                            animation: false,
                            onDone: () => {
                                // TODO: 3*2 and 3 should be related to stroke size instead of absolute
                                var bounds = RenderKanji.renderer.getLastBounds();
                                canvas.width = Math.min(bounds.x.max - bounds.x.min + 3*2, size);
                                x = -bounds.x.min + 3

                                image.width = canvas.width;

                                container.append(image);

                                if(++readySymbols == text.length) {
                                    //console.debug("rendering first symbol");
                                    onReady.shift()();
                                }
                            }
                        });
                    })(canvas, image, container, size, symbol, i);
    			}
            })(theSettings);

            RenderKanji.renderer.load(settings);
		}

        public render(args: {
            canvas: HTMLCanvasElement;
            size: number;
            symbol: string;
            animation?: boolean;
            onDone?: () => void;
            mapTo?: HTMLImageElement;
        }): void {
            if(!this.data) {
                this.queue.push({
                    args: args,
                    settings: RenderKanji.renderer.save()
                });
                return;
            }

            var code = args.symbol.charCodeAt(0);
            var kanji = this.data[code];

            if(!kanji)
                return console.error("Unknown symbol '"+args.symbol.charAt(0)+"' with code "+code+".");

            RenderKanji.renderer.render({
                canvas: args.canvas,
                size: args.size,
                kanji: kanji,
                animation: args.animation,
                onDone: args.onDone,
                mapTo: args.mapTo
            });
        }

        private parseDatabase(database: XMLDocument): void {
            var kanjivg: Node;
            for (var i = 0; i < database.childNodes.length; i++) {
                var child = database.childNodes[i];
                if(child.nodeName == 'kanjivg') {
                    kanjivg = child;
                    break;
                }
            }

            if(!kanjivg)
                return console.error("KanjiVG node not found.");

            this.data = {};
            for (var i = 0; i < kanjivg.childNodes.length; i++) {
                var child = kanjivg.childNodes[i];
                if(child.nodeName == 'kanji') {
                    var id = child.attributes.getNamedItem('id').value.substring('kvg:kanji_'.length);
                    var code = parseInt(id, 16);
                    this.data[code] = child;
                }
            }
        }
    }

    interface RendererSettings {
        color: string;
        weight: number;
        x: number;
        y: number;
    }

    class Renderer {

        private context2d: CanvasRenderingContext2D;
        private animation2d: fg.gui.canvas.Animations;
        private multiplier: number;
        private animation: boolean;

        private ani: fg.gui.canvas.AnimationQueue;

        public color: string;
        public weight: number;

        // drawing offset
        public x: number = 0;
        public y: number = 0;

        // used to crop canvas
        private minX: number;
        private maxX: number;
        private minY: number;
        private maxY: number;

        public render(args: {
            canvas: HTMLCanvasElement;
            size: number;
            kanji: Node;
            animation?: boolean;
            onDone?: () => void;
            mapTo?: HTMLImageElement;
        }): void {
            this.context2d = <CanvasRenderingContext2D>args.canvas.getContext('2d');
            this.animation2d = new fg.gui.canvas.Animations({ context2d: this.context2d, animationSpeed: 100 });
            this.multiplier = args.size / kanjivgAreaSize;
            this.animation = args.animation;

            this.minX = this.multiplier * kanjivgAreaSize;
            this.maxX = 0;
            this.minY = this.multiplier * kanjivgAreaSize;
            this.maxY = 0;

            this.context2d.save();

            this.context2d.lineCap = 'round';
            this.context2d.lineJoin = 'round';
            this.context2d.lineWidth = this.multiplier * 7;
            this.context2d.miterLimit = 4;

            if(this.color) this.context2d.strokeStyle = this.color;
            if(this.weight) this.context2d.lineWidth *= this.weight / 400;

            //console.debug("c2d.strokeStyle", c2d.strokeStyle, "this.color", this.color);

            this.ani = this.animation ? this.animation2d.startAnimation({
                mapTo: args.mapTo
            }) : null;

            this.context2d.clearRect(0, 0, this.multiplier * kanjivgAreaSize, this.multiplier * kanjivgAreaSize);
            this.renderG(args.kanji);

            if (this.animation) {
                this.ani.sleep(100 * this.multiplier).done(args.onDone || $.noop);
            } else {
                if (args.mapTo) {
                    var newSrc = args.canvas.toDataURL();
					if (args.mapTo.src != newSrc)
                        args.mapTo.src = newSrc;
                }

                if (args.onDone)
                    args.onDone();
            }

            this.context2d.restore();

            //console.debug("RenderKanji", "Renderer", "rendered a "+size+"x"+size+" symbol.");
            //this.context2d.beginPath();
            //this.context2d.rect(0, 0, this.multiplier * kanjivgAreaSize, this.multiplier * kanjivgAreaSize);
            //this.context2d.stroke();
        }

        public getLastBounds(): { x: { min: number; max: number; }; y: { min: number; max: number; }; } {
            return {
                x: {
                    min: this.minX,
                    max: this.maxX
                },
                y: {
                    min: this.minY,
                    max: this.maxY
                }
            };
        }

        private renderG(g: Node): void {
            for (var i = 0; i < g.childNodes.length; i++) {
                var child = g.childNodes[i];
                switch (child.nodeName) {
                    case 'g':
                        this.renderG(child);
                        break;
                    case 'path':
                        this.renderPath(child);
                        break;
                    case '#text':
                        break;
                    default:
                        console.warn("RenderKanji", "unknown node", child.nodeName);
                }
            }

            if (this.animation)
                this.ani.sleep(75 * this.multiplier);
        }

        private renderPath(path: Node): void {
            var c2d = this.context2d;

            if (!this.animation)
                c2d.beginPath();

            var relPosX = 0;
            var relPosY = 0;

            var lastControlPoint1X = 0;
            var lastControlPoint1Y = 0;

            var d = path.attributes.getNamedItem('d').value;
            var splitted = d.match(/[mlhvcsqtaz][^mlhvcsqtaz]+/ig);
            for (var i = 0; i < splitted.length; i++) {
                var command = splitted[i].substring(0, 1);
                var commandLow = command.toLowerCase();
                var isAbsolute = command >= 'A' && command <= 'Z';

                var n: number[] = [];
                var numbers = splitted[i].substring(1);
                var regexp = /-?[0-9.]+/g;
                var match: RegExpMatchArray;
                while((match = regexp.exec(numbers)) != null)
                    n.push(+match[0] * this.multiplier);

                if (!isAbsolute) {
                    for(var j = 0; j < n.length; j += 2) {
                        n[j + 0] = relPosX + n[j + 0];
                        n[j + 1] = relPosY + n[j + 1];

                        // TODO: this might make curve control points 'max*' and 'min*', which won't be drawn
                        if(n[j + 0] < this.minX)
                            this.minX = n[j + 0];
                        else if(n[j + 0] > this.maxX)
                            this.maxX = n[j + 0];

                        if(n[j + 1] < this.minY)
                            this.minY = n[j + 1];
                        else if(n[j + 1] > this.maxY)
                            this.maxY = n[j + 1];
                    }
                }
                relPosX = n[n.length - 2];
                relPosY = n[n.length - 1];

                for(var j = 0; j < n.length; j += 2) {
                    n[j + 0] += this.x;
                    n[j + 1] += this.y;
                }

                //console.debug("DRAW", splitted[i], command, n);
                switch (commandLow) {
                    case 'm':
                        (this.animation ? this.ani : c2d).moveTo(n[0], n[1]);
                        //console.debug("moveTo("+n[0]+", "+n[1]+");");
                        break;
                    case 'c':
                        (this.animation ? this.ani : c2d).bezierCurveTo(n[0], n[1], n[2], n[3], n[4], n[5]);
                        //console.debug("bezierCurveTo("+n[0]+", "+n[1]+", "+n[2]+", "+n[3]+", "+n[4]+", "+n[5]+");");
                        break;
                    case 's':
                        (this.animation ? this.ani : c2d).bezierCurveTo(lastControlPoint1X, lastControlPoint1Y, n[0], n[1], n[2], n[3]);
                        //console.debug("bezierCurveTo("+lastControlPoint1X+", "+lastControlPoint1Y+", "+n[2]+", "+n[3]+", "+n[4]+", "+n[5]+");");
                        break;
                    default:
                        console.warn("RenderKanji", "unknown svg command", command);
                }

                switch(commandLow) {
                    case 'c':
                        lastControlPoint1X = (relPosX + this.x)*2 - n[2];
                        lastControlPoint1Y = (relPosY + this.y)*2 - n[3];
                        break;

                    case 's':
                        lastControlPoint1X = (relPosX + this.x)*2 - n[0];
                        lastControlPoint1Y = (relPosY + this.y)*2 - n[1];
                        break;

                    default:
                        lastControlPoint1X = relPosX + this.x;
                        lastControlPoint1Y = relPosY + this.y;
                        break;
                }
            }
            //console.debug("---");

            if (this.animation) {
                //this.ani.sleep(50 * this.multiplier);
            } else {
                c2d.stroke();
            }
        }

        public save(): RendererSettings {
            return {
                color: this.color,
                weight: this.weight,
                x: this.x,
                y: this.y
            };
        }

        public load(settings: RendererSettings): void {
            this.color = settings.color;
            this.weight = settings.weight;
            this.x = settings.x;
            this.y = settings.y;
        }
    }
}
