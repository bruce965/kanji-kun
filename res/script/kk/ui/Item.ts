/// <reference path="../../defs/jquery/jquery.d.ts" />
/// <reference path="../../fg/dom/make.ts" />
/// <reference path="../Test.ts" />
/// <reference path="RenderKanji.ts" />

module kk.ui {

	var kanjivgDatabase = "/res/thirdparty/kanjivg.xml";

	export class Item {

		private static renderer: RenderKanji;

		public el: JQuery;

		constructor(test: Test, item: ItemAndIndex) {
			if (!Item.renderer)
				Item.renderer = new RenderKanji(kanjivgDatabase);

			var data = test.getData(item);

			this.el = fg.dom.make('div', {
				clazz: [
					'symbol-card',
					data.locked ? 'symbol-card-locked' : 'symbol-card-unlocked',
					data.fresh ? 'symbol-card-fresh' : 'symbol-card-tested',
					data.confident ? 'symbol-card-confident' : 'symbol-card-doubtful',
					data.mastered ? 'symbol-card-mastered' : 'symbol-card-learning',
					data.review ? 'symbol-card-review': 'symbol-card-noreview'
				],
				text: data.item.jp[0],
				attr: {
					'tabindex': '0'
				}
			});

			Item.renderer.appendTo(this.el, data.item.jp[0], false);
		}
	}
}
