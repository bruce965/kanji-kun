/// <reference path="../fg/core/StringDictionary.ts" />
/// <reference path="../defs/jquery/jquery.d.ts" />
/// <reference path="interfaces.ts" />

module kk {

	export class Test {

		public stats = new TestStats();

		constructor(
			/** List of items. */
			private items: TestItem[],
			/** Minimum percentage of good confidency items required to pass level. */
			private minPercentage: number,
			/** Items with answered correctly this number of times will be considered as good confidency. */
			private minConfidency: number,
			/** Drop in item confidency counter, in percentage, when anwered uncorrectly. */
			private confidencyDropPercentage: number,
			/** Delay between tests, in seconds, by confidency level. */
			private testDelay: number[]
   ) { }

		/** Get the list of items, filtered. */
		public getItems(status: ItemStatus, level?: number): ItemAndIndex[] {
			var items: ItemAndIndex[] = [];

			var itemsByLevel = this.getItemsByLevel(level);
			for (var i = 0; i < itemsByLevel.length; i++) {
				var index = itemsByLevel[i].index;
				var item = itemsByLevel[i].item;
				var stats = this.stats.get(index);

				if (this.itemMatches(status, item, stats))
					items.push({ index: index, item: item });
			}

			return items;
		}

		/** Get data about one item. */
		public getData(item: ItemAndIndex): TestItemData {
			var stats = this.stats.get(item.index);

			return {
				index: item.index,
				item: item.item,

				stats: stats,
				nextTest: new Date(this.nextReview(item.item, stats)),

				locked: this.itemMatches(ItemStatus.locked, item.item, stats),
				fresh: this.itemMatches(ItemStatus.fresh, item.item, stats),
				confident: this.itemMatches(ItemStatus.confident, item.item, stats),
				mastered: this.itemMatches(ItemStatus.mastered, item.item, stats),
				review: this.itemMatches(ItemStatus.review, item.item, stats)
			};
		}

		// Check if the user can level up and update stats.
		private update(): void {
			for (var lv = 0; lv <= this.stats.level + 1; lv++) {
				var total = this.getItemsCount(lv);
				var confident = total - this.getItems(ItemStatus.confident, lv).length;

				if (confident / total < this.minPercentage)
					break;
			}

			this.stats.level++;
		}

		// level null = any level
		private getItemsByLevel(level?: number): ItemAndIndex[] {
			var items: ItemAndIndex[] = [];
			for (var i = 0; i < this.items.length; i++) {
				var item = this.items[i];

				if (!level || item.lv == level)
					items.push({ index: i, item: item });
			}

			return items;
		}

		private getItemsCount(level: number): number {
			var count;
			for (var k in this.items)
				if (this.items[k].lv == level)
					count++;
			return count;
		}

		private itemMatches(status: ItemStatus, item: TestItem, stats: TestItemStats): boolean {
			if (status & ItemStatus.locked && item.lv <= this.stats.level + 1) return false;
			if (status & ItemStatus.unlocked && item.lv > this.stats.level + 1) return false;

			if (status & ItemStatus.fresh && stats.lastTest) return false;
			if (status & ItemStatus.tested && !stats.lastTest) return false;

			if (status & ItemStatus.doubtful && stats.userConfidency >= this.minConfidency) return false;
			if (status & ItemStatus.confident && stats.userConfidency < this.minConfidency) return false;

			if (status & ItemStatus.learning && stats.userConfidency >= this.testDelay.length) return false;
			if (status & ItemStatus.mastered && stats.userConfidency < this.testDelay.length) return false;

			if (status & ItemStatus.review && !this.nextReview(item, stats)) return false;
			if (status & ItemStatus.noreview && this.nextReview(item, stats)) return false;

			return true;
		}

		private nextReview(item: TestItem, stats: TestItemStats): number {
			if (
				this.itemMatches(ItemStatus.mastered, item, stats) ||
				this.itemMatches(ItemStatus.locked, item, stats)
			)
			return 0;

			var delay = this.testDelay[stats.userConfidency] * 1000;
			var now = new Date().getTime();
			var last = stats.lastTest ? stats.lastTest.getTime() : 0;

			if(last + delay > now)
				return 0;

			return last + delay;
		}

		public serialize(): any {
			this.update();
			return this.stats.serialize();
		}

		public deserialize(serialized: any): void {
			this.stats.deserialize(serialized);
		}
	}

	export enum ItemStatus {
		any = 0,

		locked = 1,  // item unknown and level too high (excludes unlocked)
		unlocked = 2,  // item available for learning or testing (excludes locked)

		fresh = 4,  // item never tested before (excludes tested)
		tested = 8,  // item already tested (implies unlocked, excludes fresh)

		doubtful = 16,  // user not confident with the item yet
		confident = 32,  // user confident with the item (implies tested, excludes doubtful)

		learning = 64,  // user didn't pass all tests on the item yet (excludes mastered)
		mastered = 128,  // user passed all tests on the item (implies confident, excludes doubtful, learning)

		review = 256,  // item should be reviewed now (implies unlocked, learning, excludes noreview)
		noreview = 512,  // no review planned for now (excludes review)
	}

	export interface ItemAndIndex {
		index: number;
		item: TestItem;
	}

	export interface TestItemData extends ItemAndIndex {
		stats: TestItemStats;
		nextTest: Date;

		locked: boolean;
		fresh: boolean;
		confident: boolean;
		mastered: boolean;
		review: boolean;
	}

	export interface TestItem extends Translatable, LevelItem { }

	export interface TestItemStats {
		lastTest: Date;
		passedTests: number;
		failedTests: number;
		userConfidency: number;	// an integer between 0 and infinity, increases by one per passed test
	}

	export class TestStats {

		public level: number = 0;

		private dic: fg.core.StringDictionary<TestItemStats> = {};

		public get(key: number): TestItemStats {
			if (!this.dic.hasOwnProperty(key.toString())) {
				this.dic[key] = {
					lastTest: null,
					passedTests: 0,
					failedTests: 0,
					userConfidency: 0
				};
			}

			return this.dic[key];
		}

		public serialize(): any {
			var d = {};
			for (var k in this.dic) {
				var v = this.dic[k];
				if (!v.lastTest) continue;
				d[k] = {
					l: v.lastTest.getTime(),
					v: v.passedTests || undefined,
					x: v.failedTests || undefined,
					s: v.userConfidency || undefined
				};
			}

			return { d: d, l: this.level || undefined };
		}

		public deserialize(serialized: any): void {
			this.dic = {};
			this.level = serialized.level || 0;
			for (var k in serialized.d) {
				var v = serialized[k];
				this.dic[k] = {
					lastTest: new Date(v.l),
					passedTests: v.v || 0,
					failedTests: v.x || 0,
					userConfidency: v.s || 0
				};
			}
		}
	}
}
