/// <reference path="Test.ts" />
/// <reference path="hiragana.ts" />

module kk {

	var t_immediate = 0;
	var t_1m = 60;
	var t_5m = t_1m * 5;
	var t_10m = t_1m * 10;
	var t_15m = t_1m * 15;
	var t_30m = t_1m * 30;
	var t_1h = t_1m * 60;
	var t_2h = t_1h * 2;
	var t_4h = t_1h * 4;
	var t_8h = t_1h * 8;
	var t_1d = t_1h * 24;
	var t_2d = t_1d * 2;
	var t_3d = t_1d * 3;
	var t_1w = t_1d * 7;

	export class UserStats {

		public hiragana = new Test(hiragana, 1.00, 4, 0.25, [t_immediate, t_1m, t_5m, t_10m, t_15m, t_30m, t_1h, t_2h, t_4h, t_8h, t_1d, t_2d, t_3d, t_1w]);

		constructor() { }

		public serialize(): any {
			return {
				h: this.hiragana.serialize()
			};
		}

		public deserialize(serialized: any): void {
			this.hiragana.deserialize(serialized.h);
		}
	}
}
