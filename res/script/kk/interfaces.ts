
module kk {

	export interface Translatable {
		jp: string[];
		en: string[];
	}

	export interface LevelItem {
		lv: number;
	}
}
