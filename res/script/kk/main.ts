/// <reference path="../defs/jquery/jquery.d.ts" />
/// <reference path="../fg/core/WebStorage.ts" />
/// <reference path="ui/Item.ts" />
/// <reference path="UserStats.ts" />

module kk {

	var currentSymbol: JQuery;
	var currentText: JQuery;

	var user = new UserStats();

	export function main(): void {
		currentSymbol = $('#currentSymbol');
		currentText = $('#currentText');

		if(fg.core.WebStorage.local.contains('kanjikun'))
			user.deserialize(fg.core.WebStorage.local.get('kanjikun'));

		var items = user.hiragana.getItems(ItemStatus.any);

		var locked = user.hiragana.getItems(ItemStatus.locked);
		console.log("Locked: ", locked);
		console.log("New: ", user.hiragana.getItems(ItemStatus.fresh | ItemStatus.unlocked));
		console.log("Unclear: ", user.hiragana.getItems(ItemStatus.tested | ItemStatus.doubtful));
		console.log("Review: ", user.hiragana.getItems(ItemStatus.tested | ItemStatus.review));
		console.log("Confident: ", user.hiragana.getItems(ItemStatus.learning | ItemStatus.confident));
		console.log("Mastered: ", user.hiragana.getItems(ItemStatus.mastered));

		for(var i = 0; i < items.length; i++) {
			var item = new ui.Item(user.hiragana, items[i]);
			currentText.append(item.el);
		}
	}
}
