
module fg.core {

    /** A standard JavaScript string-keyed dictionary. */
    export interface StringDictionary<T> {

        /** Get or set a value by key. */
        [key: string]: T;

        // TODO: uncomment as soon as this bug in TypeScript is fixed
        /** Determine if this dictionary contains the specified key. */
        //hasOwnProperty(key: string): boolean;
    }
}
