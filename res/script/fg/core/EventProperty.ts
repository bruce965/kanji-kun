/// <reference path="Event.ts" />
/// <reference path="Dictionary.ts" />
/// <reference path="Property.ts" />

module fg.core {

	/** Create an event property. */
	export function property<T>(value?: T): EventProperty<T> {
		return new EventProperty<T>(value);
	}

	/** A property with a 'change' event. */
	export class EventProperty<T> implements Property<T> {

		/** Event fired when the value of this property changes. */
		public onChange = event<(value: T) => void>(this);

		constructor(private value?: T) { }

		private onValueEvents = new Dictionary<T, core.Event<(value: T) => void>>();
		/** Add an event listener for when this property assumes a specific value. */
		public onValue(value: T): core.Event<(value: T) => void> {
			var ev = this.onValueEvents.get(value);
			if (!ev) {
				ev = core.event(this.value);
				this.onValueEvents.set(value, ev);
			}
			return ev;
		}

		public get(): T {
			return this.value;
		}

		public set(value: T): void {
			var changed = this.value !== value;
			this.value = value;

			if (changed) {
				this.onChange.fire(value);

				if (this.onValueEvents.count()) {
					var ev = this.onValueEvents.get(value);
					if (ev)
						ev.fire(value);
				}
			}
		}
	}
}
