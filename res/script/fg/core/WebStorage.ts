
module fg.core {

    /** Provides access to web storage. */
    export class WebStorage {

        /** Storage persisting between sessions. */
        public static local = new WebStorage(false);

        /** Storage emptied when the browser is closed. */
        public static session = new WebStorage(true);

        private storage: Storage;

        constructor(session: boolean = false) {
            this.storage = session ? sessionStorage : localStorage;
        }

        /** Set a value with the specified key to the storage. */
        public set(key: string, value: any): void {
            this.storage.setItem(key, JSON.stringify(value));
        }

        /** Get a value from the storage. */
        public get(key: string): any {
            return JSON.parse(this.storage.getItem(key));
        }

        /** Delete a value from the storage. */
        public remove(key: string): void {
            this.storage.removeItem(key);
        }

        /** Check if the storage contains a value with the specified key. */
        public contains(key: string): boolean {
            return this.storage.getItem(key) !== null;
        }

        /**
         * Get an approximate number of JSON characters that can still be added
         * to this storage, returns 0 if this storage is not supported.
         */
        public freeSpace(): number {
            if(!this.storage)
                return 0;

            var i = 0;
            try {
                for (i = 250; i <= 20000; i += 250)
                    this.storage.setItem('__wsfreespace_test', new Array((i * 1024) + 1).join('a'));
            } catch (e) {
                this.storage.removeItem('__wsfreespace_test');
                return (i - 250) * 1024;
            }
        }
    }
}
