/// <reference path="../../defs/jquery/jquery.d.ts" />
/// <reference path="../dom/make.ts" />
/// <reference path="../gui/input/Mouse.ts" />
/// <reference path="../util/Delay.ts" />
/// <reference path="../util/uid.ts" />

module fg.gui {

    /** An item in a context menu. */
    export interface ContextMenuItem {
        label?: string;
        icon?: string;
        action?: () => void;
        child?: (ContextMenuItem|ContextMenuSeparator)[];
    }

    /** A context menu separating line. */
    export class ContextMenuSeparator { }

    var contextMenuClass = 'contextmenu';
    var contextMenuListClass = 'contextmenu-list';
    var contextMenuSeparatorClass = 'contextmenu-separator';
    var contextMenuItemClass = 'contextmenu-item';
    var contextMenuItemImageClass = 'contextmenu-item-image';
    var contextMenuItemLabelClass = 'contextmenu-item-label';
    var contextMenuItemDisabledClass = 'contextmenu-item-disabled';
    var contextMenuItemExpandableClass = 'contextmenu-item-expandable';
    var contextMenuItemHoverClass = 'contextmenu-item-hover';

    /** A cross-browser custom context menu. */
    export class ContextMenu {

        /** A context menu separating line. */
        public static separator = new ContextMenuSeparator();

        private menu: JQuery;
        private list: JQuery;

        private delay = new util.Delay(500);
        private lastEventTimestamp: number;
        private plannedSubmenu: ContextMenu;
        private currentSubmenu: ContextMenu;
        private plannedSubmenuItem: JQuery;
        private currentSubmenuItem: JQuery;

        private parent: ContextMenu;

        constructor() {
            this.menu = dom.make('div', {
                clazz: [contextMenuClass],
                style: {
                    position: 'fixed'
                },
                on: {
                    contextmenu: e => e.preventDefault()
                },
                child: [
                    this.list = dom.make('ul', { clazz: [contextMenuListClass], attr: { role: 'listbox' } })
                ]
            });

            this.delay.onDelay.bind(() => {
                if (!this.isOpen())
                    return;

                if (this.plannedSubmenu == this.currentSubmenu && this.currentSubmenu && this.currentSubmenu.isOpen())
                    return;

                if (this.currentSubmenu) {
                    this.currentSubmenu.close();
                    this.currentSubmenu = null;
                }

                if (!this.plannedSubmenu)
                    return;

                this.plannedSubmenu.openAt(0, 0);  // required to compute actual position

                var x = this.plannedSubmenuItem.offset().left + this.menu.width() - $(window).scrollLeft();
                var y = this.plannedSubmenuItem.offset().top - $(window).scrollTop();

                if (x + this.plannedSubmenu.menu.outerWidth() > $(window).width()) {
                    x = this.plannedSubmenuItem.offset().left - this.plannedSubmenu.menu.outerWidth();

                    if (x < 0)
                        x = 0;
                }

                if (y + this.plannedSubmenu.menu.outerHeight() > $(window).height()) {
                    y = $(window).height() - this.plannedSubmenu.menu.outerHeight();

                    if (y < 0)
                        y = 0;
                }

                this.plannedSubmenu.openAt(x, y);
                this.currentSubmenu = this.plannedSubmenu;
                this.currentSubmenuItem = this.plannedSubmenuItem;

                this.plannedSubmenu = null;
                this.plannedSubmenuItem = null;
            });
        }

        /** Set or update the content of this context menu. */
        public setData(data: ContextMenuItem): void {
            if (data.child)
                for (var i = 0; i < data.child.length; i++)
                    this.createMenuItem(data.child[i]).appendTo(this.list);
        }

        private createMenuItem(data: ContextMenuItem|ContextMenuSeparator): JQuery {
            var item = dom.make('li', { attr: { role: 'option' } });

            if (data instanceof ContextMenuSeparator)
                return item.addClass(contextMenuSeparatorClass).append(dom.make('hr'));
            else
                item.attr('tabindex', '-1').addClass(contextMenuItemClass);

            var itemData = <ContextMenuItem>data;

            if (itemData.label)
                item.append(dom.make('span', { clazz: [contextMenuItemLabelClass], text: itemData.label }));

            if (itemData.icon)
                item.append(dom.make('img', { clazz: [contextMenuItemImageClass], prop: { src: itemData.icon, alt: '' } }));

            if (itemData.child) {
                item.addClass(contextMenuItemExpandableClass);

                if (itemData.action) {
                    console.warn("ContextMenu items cannot have both 'action' and 'child' specified.");
                } else {
                    var submenu = new ContextMenu();
                    submenu.parent = this;
                    submenu.setData(itemData);

                    ((submenu: ContextMenu, item: JQuery) => {
                        item.click(() => {
                            this.plannedSubmenu = submenu;
                            this.plannedSubmenuItem = item;
                            this.delay.fire();
                        });

                        item.on('mousemove touchmove', e => {
                            this.plannedSubmenu = submenu;
                            this.lastEventTimestamp = e.timeStamp;
                            this.plannedSubmenuItem = item;
                            this.delay.set();
                        });
                    })(submenu, item);
                }
            } else if (itemData.action) {
                item.click(() => {
                    if (this.parent)
                        this.parent.close();
                    else
                        this.close();

                    itemData.action();
                });
            } else {
                item.addClass(contextMenuItemDisabledClass);
                item.attr('aria-disabled', 'true');
            }

            return item;
        }

        private isCurrentlyOpen = false;
        /** Check wether this context menu is currently open or not. */
        public isOpen(): boolean {
            return this.isCurrentlyOpen;
        }

        /** Open this context menu at the specified client location (default is mouse position). */
        public open(x: number = gui.input.mouse.clientX, y: number = gui.input.mouse.clientY): void {
            this.openAt(0, 0);  // required to compute actual position

            if (x + this.menu.outerWidth() > $(window).width()) {
                x = $(window).width() - this.menu.outerWidth();

                if (x < 0)
                    x = 0;
            }

            if (y + this.menu.outerHeight() > $(window).height()) {
                y = $(window).height() - this.menu.outerHeight();

                if (y < 0)
                    y = 0;
            }

            this.openAt(x, y);
        }

        private closeInstance = () => this.close();
        /** Close this context menu. */
        public close(): void {
            this.isCurrentlyOpen = false;

            if (this.currentSubmenu && this.currentSubmenu.isOpen())
                this.currentSubmenu.close();

            this.menu.detach();

            $(window).off('scroll resize', this.closeInstance);
            $(document).off('keydown', this.onKeyDownInstance);
            $(document).off('mousemove touchmove', this.onMouseMoveInstance);
            $(document).off('touchstart mousedown', this.onMouseDownInstance);
        }

        private openAt(x: number, y: number): void {
            //console.debug('openAt', x, y);

            if (this.isCurrentlyOpen)
                this.close();

            this.isCurrentlyOpen = true;

            this.menu.css({ top: y, left: x }).appendTo(document.body);
            this.focusItem(null);

            $(window).on('scroll resize', this.closeInstance);
            $(document).on('keydown', this.onKeyDownInstance);
            $(document).on('mousemove touchmove', this.onMouseMoveInstance);
            $(document).on('touchstart mousedown', this.onMouseDownInstance);
        }

        private onKeyDownInstance = (e: JQueryKeyEventObject) => this.onKeyDown(e);
        private onKeyDown(e: JQueryKeyEventObject): void {
            //console.debug('onKeyDown', e.which);

            if (this.currentSubmenu && this.currentSubmenu.isOpen()) {
                if (e.which == 37)  // ARROW LEFT
                    this.currentSubmenu.close();
                return;
            }

            if (e.which == 27)  // ESCAPE
                return this.close();

            var items = this.getValidItems();

            var focused = this.menu.find('.' + contextMenuItemHoverClass);
            var isFocused = !!focused.length;
            if (!isFocused)
                focused = items.first();

            if (e.which == 13) {  // ENTER
                if (isFocused) {
                    focused.click();
                    this.close();
                } else {
                    this.focusItem(items.first());
                }

                return;
            }

            if (e.which == 9) { // TAB
                this.closeAll();
                return;
            }

            if (e.which == 39) {  // ARROW RIGHT
                if (focused.is('.' + contextMenuItemExpandableClass)) {
                    focused.click();
                    this.currentSubmenu.focusFirstItem();
                } else if (!isFocused) {
                    this.focusItem(items.first());
                }
                return;
            }

            if (e.which == 38 || e.which == 40) {
                if (!isFocused) {
                    this.focusItem(items.first());
                } else {
                    var index = items.index(focused);

                    if (e.which == 38) {  // ARROW UP
                        if (index == 0)
                            this.focusItem(items.last());
                        else
                            this.focusItem($(items.get(index - 1)));
                    } else {  // ARROW DOWN
                        if (index == items.length - 1)
                            this.focusItem(items.first());
                        else
                            this.focusItem($(items.get(index + 1)));
                    }
                }

                e.preventDefault();
                return;
            }
        }

        private onMouseMoveInstance = (e: JQueryEventObject) => this.onMouseMove(e);
        private onMouseMove(e: JQueryEventObject): void {
            //console.debug("onMouseMove", e.target);

            if (this.isThis(e.target)) {
                // abort submenu open timeout if we move the mouse inside the menu
                if (this.lastEventTimestamp != e.timeStamp) {
                    this.lastEventTimestamp = e.timeStamp;
                    this.plannedSubmenu = null;
                    this.delay.reset();
                }

                var item = $(e.target).closest('.' + contextMenuItemClass + ':not(.' + contextMenuItemDisabledClass + ')');
                if (item.length) {
                    this.focusItem(item);
                    return;
                }
            } else {
                // don't open last focused submenu if we move mouse outside menu
                if (this.lastEventTimestamp != e.timeStamp)
                    this.delay.abort();
            }

            if (this.currentSubmenu && this.currentSubmenu.isOpen() && !this.delay.isSet())
                this.focusItem(this.currentSubmenuItem);
            else
                this.focusItem(null);
        }

        private onMouseDownInstance = (e: JQueryEventObject) => this.onMouseDown(e);
        private onMouseDown(e: JQueryEventObject): void {
            // prevent closing parent menu when clicking on self or a submenu
            if (!this.isThis(e.target, true))
                this.close();
        }

        private getValidItems(): JQuery {
            return this.list.find('.' + contextMenuItemClass + ':not(.' + contextMenuItemDisabledClass + ')');
        }

        private focusFirstItem(): void {
            this.focusItem(this.getValidItems().first());
        }

        private focusItem(item: JQuery): void {
            this.list.find('.' + contextMenuItemHoverClass).removeClass(contextMenuItemHoverClass);

            if (item)
                item.addClass(contextMenuItemHoverClass);
        }

        private isThis(e: Element, alsoSubmenus?: boolean): boolean {
            if (this.menu.is(e) || this.menu.has(e).length)
                return true;

            if (alsoSubmenus && this.currentSubmenu)
                return this.currentSubmenu.isThis(e, true);

            return false;
        }

        private closeAll(): void {
            if (this.parent)
                this.parent.closeAll();
            else
                this.close();
        }
    }
}
