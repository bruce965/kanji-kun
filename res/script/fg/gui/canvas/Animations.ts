/// <reference path="AnimationQueue.ts" />
/// <reference path="CanvasState.ts" />

module fg.gui.canvas {

	export class Animations {

		private context2d: CanvasRenderingContext2D;
		private animationSpeed: number;
		private epsilon: number;

		// last move to
		private x: number;
		private y: number;

		// last drawn point
		private lastX: number;
		private lastY: number;

		constructor(args: {
			context2d: CanvasRenderingContext2D;

			/** Animation speed in pixels/second. (default: 150) */
			animationSpeed?: number;

			/** Lower values mean higher precision, but more computations. (deafult: 0.1) */
			epsilon?: number;
		}) {
			this.context2d = args.context2d;
			this.animationSpeed = args.animationSpeed || 150;
			this.epsilon = args.epsilon || 0.1;
		}

		public startAnimation(args?: {
			mapTo: HTMLImageElement;
		}): AnimationQueue {
			return new AnimationQueue({
				context2d: this.context2d,
				animation2d: this,
				animationSpeed: this.animationSpeed,
				mapTo: args ? args.mapTo : null
			});
		}

		public moveToLength(x: number, y: number): number {
			return Math.sqrt((this.lastX - x)*(this.lastX - x) + (this.lastY - y)*(this.lastY - y));
		}

		public moveTo(x: number, y: number): void {
			//console.debug("moveTo");

			this.x = x;
			this.y = y;

			this.lastX = x;
			this.lastY = y;
		}

		public bezierCurveLength(cp1x: number, cp1y: number, cp2x: number, cp2y: number, x: number, y: number): number {
			var lastX = this.lastX;
			var lastY = this.lastY;

			var sum = 0;
			for (var t = 0; true; t += this.epsilon) {
				if (t > 1)
					t = 1;

				var newX = lastX;
				var newY = lastY;

				lastX = Math.pow(1 - t, 3) * this.x +
					3 * t * Math.pow(1 - t, 2) * cp1x +
					3 * Math.pow(t, 2) * (1 - t) * cp2x +
					Math.pow(t, 3) * x;

				lastY = Math.pow(1 - t, 3) * this.y +
					3 * t * Math.pow(1 - t, 2) * cp1y +
					3 * Math.pow(t, 2) * (1 - t) * cp2y +
					Math.pow(t, 3) * y;

				sum += Math.sqrt((lastX - newX)*(lastX - newX) + (lastY - newY)*(lastY - newY));

				if(t == 1)
					break;
			}

			return sum;
		}

		public bezierCurveTo(cp1x: number, cp1y: number, cp2x: number, cp2y: number, x: number, y: number, start: number, end: number): void {
			//console.debug("bezierCurveTo", start, end);

			this.context2d.beginPath();
			this.context2d.moveTo(this.lastX, this.lastY);

			for (var t = start; true; t += this.epsilon) {
				if (t > end)
					t = end;

				this.context2d.lineTo(
					this.lastX = Math.pow(1 - t, 3) * this.x +
					3 * t * Math.pow(1 - t, 2) * cp1x +
					3 * Math.pow(t, 2) * (1 - t) * cp2x +
					Math.pow(t, 3) * x,

					this.lastY = Math.pow(1 - t, 3) * this.y +
					3 * t * Math.pow(1 - t, 2) * cp1y +
					3 * Math.pow(t, 2) * (1 - t) * cp2y +
					Math.pow(t, 3) * y
				);

				if(t == end)
					break;
			}

			this.context2d.stroke();
		}
	}
}
