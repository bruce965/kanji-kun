
module fg.gui.canvas {

	export interface CanvasState {
		strokeStyle: string;
		fillStyle: string;
		globalAlpha: number;
		lineWidth: number;
		lineCap: string;
		lineJoin: string;
		miterLimit: number;
		shadowOffsetX: number;
		shadowOffsetY: number;
		shadowBlur: number;
		shadowColor: string;
		globalCompositeOperation: string;
		font: string;
		textAlign: string;
		textBaseline: string;
	}

	export function saveState2D(c2d: CanvasRenderingContext2D): CanvasState {
		return {
			strokeStyle: c2d.strokeStyle,
			fillStyle: c2d.fillStyle,
			globalAlpha: c2d.globalAlpha,
			lineWidth: c2d.lineWidth,
			lineCap: c2d.lineCap,
			lineJoin: c2d.lineJoin,
			miterLimit: c2d.miterLimit,
			shadowOffsetX: c2d.shadowOffsetX,
			shadowOffsetY: c2d.shadowOffsetY,
			shadowBlur: c2d.shadowBlur,
			shadowColor: c2d.shadowColor,
			globalCompositeOperation: c2d.globalCompositeOperation,
			font: c2d.font,
			textAlign: c2d.textAlign,
			textBaseline: c2d.textBaseline
		};
	}

	export function restoreState2D(c2d: CanvasRenderingContext2D, state: CanvasState): void {
		c2d.strokeStyle = state.strokeStyle;
		c2d.fillStyle = state.fillStyle;
		c2d.globalAlpha = state.globalAlpha;
		c2d.lineWidth = state.lineWidth;
		c2d.lineCap = state.lineCap;
		c2d.lineJoin = state.lineJoin;
		c2d.miterLimit = state.miterLimit;
		c2d.shadowOffsetX = state.shadowOffsetX;
		c2d.shadowOffsetY = state.shadowOffsetY;
		c2d.shadowBlur = state.shadowBlur;
		c2d.shadowColor = state.shadowColor;
		c2d.globalCompositeOperation = state.globalCompositeOperation;
		c2d.font = state.font;
		c2d.textAlign = state.textAlign;
		c2d.textBaseline = state.textBaseline;
	}
}
