/// <reference path="Animations.ts" />
/// <reference path="CanvasState.ts" />

module fg.gui.canvas {

	var noop = () => {};

	export class AnimationQueue {

		private context2d: CanvasRenderingContext2D;
		private animation2d: Animations;
		private animationSpeed: number;
		private mappedTo: HTMLImageElement;

		private animationRunning = false;
		private queue: (() => boolean)[] = [];

		constructor(args: {
			context2d: CanvasRenderingContext2D;
			animation2d: Animations;
			animationSpeed: number;	// pixels/second
			mapTo?: HTMLImageElement;
		}) {
			this.context2d = args.context2d;
			this.animation2d = args.animation2d;
			this.animationSpeed = args.animationSpeed;
			this.mappedTo = args.mapTo;
		}

		public moveTo(x: number, y: number): AnimationQueue {
			this.push(
				saveState2D(this.context2d),
				() => { this.animation2d.moveTo(x, y); },
				() =>this.animation2d.moveToLength(x, y)
			);

			return this;
		}

		public sleep(pixels: number): AnimationQueue {
			this.push(null, noop, () => pixels);

			return this;
		}

		public bezierCurveTo(cp1x: number, cp1y: number, cp2x: number, cp2y: number, x: number, y: number): AnimationQueue {
			this.push(
				saveState2D(this.context2d),
				(start: number, end: number) => { this.animation2d.bezierCurveTo(cp1x, cp1y, cp2x, cp2y, x, y, start, end); },
				() => this.animation2d.bezierCurveLength(cp1x, cp1y, cp2x, cp2y, x, y)
			);

			this.moveTo(x, y);

			return this;
		}

		public done(func: () => void): AnimationQueue {
			this.push(null, () => { func(); });

			return this;
		}

		private lastTime: number;
		private secondsPassed(): number {
			var currentTime = new Date().getTime();
			var timePassed = currentTime - this.lastTime;
			this.lastTime = currentTime;

			return timePassed / 1000;
		}

		private push(state: CanvasState, func: (start: number, end: number) => void, lengthf?: () => number): void {
			(() => {
				var duration: number;
				var length: number;
				var secondsPassedTotal = 0;
				var lastT = 0;

				//console.debug("push", "length="+length, "duration="+duration, func.toString());
				this.queue.push(() => {
					if(duration == null) {
						length = lengthf ? lengthf() : 0;
						duration = length / this.animationSpeed;
					}

					var secondsPassed = this.secondsPassed();
					secondsPassedTotal += secondsPassed;
					var t = secondsPassedTotal / duration;

					var lastState: CanvasState;
					if (state) {
						lastState = saveState2D(this.context2d);
						restoreState2D(this.context2d, state);
					}

					func(lastT, Math.min(1, t));
					lastT = t;

					if (state)
						restoreState2D(this.context2d, lastState);

					return secondsPassedTotal < duration;
				});
			})();

			if (!this.animationRunning)
				this.shift();
		}

		private shift(): void {
			if (this.animationRunning || this.queue.length == 0)
			 	return;

			// seconds passed called so that time counter is reset
			this.secondsPassed();
			this.animationRunning = true;

			var anim = this.queue[0];
			var frame = () => {
				if(anim()) {
					//setTimeout(frame, 1000 / 60);
					requestAnimationFrame(frame)
				} else {
					this.queue.shift();
					//console.debug("shift");
					this.animationRunning = false;
					this.shift();
				}

				if(this.mappedTo) {
					var newSrc = this.context2d.canvas.toDataURL();
					if(this.mappedTo.src != newSrc)
						this.mappedTo.src = newSrc;
				}
			};
			frame();
		}
	}
}
