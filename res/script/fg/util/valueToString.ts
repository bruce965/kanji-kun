
module fg.util {

    // --- STRING FORMAT SPECS ---
    // type
    // (example: number, boolean, string, undefined)
    //
    // baseValue
    // (example: 1, 2, 3.4, false, true, a, ab, abc)
    //
    // baseString = type + "." + (baseValue | constructorName)
    // (example: 3.14 = number.3.14, "hello" = string.hello, {} = 'object.Object')
    //
    // properties = "[" + prop1 + "|" + prop2 + "|" + ... + "]"
    // (example: { a: 2, b: 3, c: 4 } = [a|b|c])
    //
    // values = "{" + baseString1 + "|" + baseString2 + "|" + ... + "}"
    // (example: { a: 2, b: 3, c: 4 } = {number.2|number.3|number4})
    //
    // composedString = baseString + properties + "." + values
    // (example: { a: 2, b: 3, c: 4 } = object.Object[a|b|c].{number.2|number.3|number4})

    /**
     * Convert a value to a string, the same value always returns the same string,
     * two different values never return the same string.
     *
     * If this function is invoked for objects, two objects with the same property
     * values in different order will return the same string.
     * Undefined properties behave the same as missing properties, but differently
     * from null properties.
     *
     * The returned string doesn't differentiate two equivalent values from two
     * different classes with the same name.
     *
     * Supported types: any custom object, number, boolean, string, undefined, null,
     * RegExp, Date, anonymous objects, objects implementing "valueOf" returning base
     * type even if recursive.
     *
     * Unsupported types: recursive types, functions, Buffer, Int*Array, UInt*Array,
     * Float*Array, other esoteric builtin types.
     */
    export function valueToString(value: any) {
        var type = typeof value;
        if (Array.isArray ? Array.isArray(value) : value instanceof Array) {
            return arrayToString(value);
        } else if (type === 'object') {
            if (value === null)
                return "js.object.null";

            return objectToString(value);
        } else {
            return baseValueToString(value);
        }
    }

    function baseValueToString(value: any) {
        return (typeof value) + '.' + value;
    }

    function arrayToString(value: any[]) {
        var string = 'js.array.{';

        for (var i = 0; i < value.length; i++)
            string += (i ? '|' : '') + valueToString(value[i]);

        return string + '}';
    }

    function objectToString(value: Object) {
        if (typeof value.valueOf === 'function') {
            var valueOf = value.valueOf()

            if(typeof valueOf !== 'object' && !(Array.isArray ? Array.isArray(value) : value instanceof Array)) {
                var constructorName = getConstructorName(value) || 'unknown';
                return 'object.' + constructorName + '.valueOf.' + valueToString(value.valueOf());
            }
        }

        if (value instanceof RegExp)
            return 'js.RegExp.' + value;

        if (value instanceof Date)
            return 'js.Date.' + (<Date>value).getTime();

        var properties = [];
        for (var k in value)
            if (value[k] !== undefined)
                properties.push(k);
        properties.sort();

        var values = [];
        for (var i = 0; i < properties.length; i++)
            values[i] = valueToString(value[properties[i]]);

        var constructorName = getConstructorName(value) || 'unknown';

        return 'object.' + constructorName + (properties.length ? ('[' + properties.join('|') + '].{' + values.join('|') + '}') : '');
    }

    var funcNameRegex = /function (.{1,})\(/;
    function getConstructorName(value: Object) {
        if (typeof value.constructor['name'] === 'string')
            return value.constructor['name'];

        var results = funcNameRegex.exec(value.constructor.toString());
        return (results && results.length > 1) ? results[1] : '';
    }
}
